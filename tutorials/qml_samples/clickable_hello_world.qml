//	http://doc.qt.io/qt-5/qmlfirststeps.html

import QtQuick 2.3

Rectangle {
    width: 200
    height: 100
    color: "red"

    Text {
        anchors.centerIn: parent
        text: "Hello, World!"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: parent.color = "blue"
    }

	focus: true
    Keys.onPressed: {
        if (event.key == Qt.Key_Return) {
            color = "blue";
            event.accepted = true;
        }
    }
}