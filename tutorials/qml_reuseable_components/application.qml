//	http://doc.qt.io/qt-5/qmlfirststeps.html

// application.qml
import QtQuick 2.3

Column {
    Button { width: 50; height: 50 }
    Button { x: 50; width: 100; height: 50; color: "blue" }
    Button { width: 50; height: 50; radius: 8 }
}