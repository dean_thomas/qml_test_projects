import QtQuick 2.0
import QtQuick.Controls 2.3
import test 1.0

ApplicationWindow {
    visible: true

   PieChart {
       id: aPieChart
       anchors.centerIn: parent
       width: 100; height: 100
       name: "A simple pie chart"
       color: model.color

       Binding {
           target: model
           property: "color"
           value: aPieChart.color
       }
   }

   Button {
       onClicked:{
           var colors = ["blue", "green", "red", "yellow", "cyan", "magenta"];
           var color = colors[Math.floor(Math.random() * colors.length)]
           console.log("Random color selected is " + color);
           return aPieChart.color = color;
       }
   }
}
