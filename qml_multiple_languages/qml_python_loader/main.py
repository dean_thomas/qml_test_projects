#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8
import sys
import os
from random import choice
from PySide2.QtGui import QGuiApplication
from PySide2.QtCore import QObject, QUrl, Property, Signal
from PySide2.QtQml import QQmlApplicationEngine, QQmlDebuggingEnabler
import logging
logging.basicConfig(level=logging.DEBUG)


class PieChart(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._color = choice([
            "blue",
            "green",
            "red",
            "yellow",
            "cyan",
            "magenta"
        ])

    def __repr__(self):
        return f"Color is: {self.color}"

    colorChanged = Signal(str)

    @Property(str, notify=colorChanged)
    def color(self):
        return self._color

    @color.setter
    def set_color(self, value):
        if self._color != value:
            self._color = value
            logging.debug(f"Color was set to {value}")
            self.colorChanged.emit(value)
        else:
            logging.debug(f"Color is already {value}, so ignoring.")


#   QML Plugin path is where the share library should be placed when built
#
#   example with PLUGIN_PATH set to os.path.expanduser("~/my_plugin_dir"):
#
#       ~/my_plugin_dir
#           |
#           |- test_plugin
#           |   |
#           |   |- qmldir
#           |   |- libmyplugin1.so / myplugin1.dll
#           |   |- libmyplugin2.so / myplugin2.dll
#           |
#           |- another_plugin
#
PLUGIN_PATH = os.path.expanduser("~/my_plugin_dir")

if __name__ == "__main__":
    QQmlDebuggingEnabler()
    
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    engine.addImportPath(PLUGIN_PATH)

    logging.debug(f"QML import pathlist: {engine.importPathList()}")

    m = PieChart()
    logging.debug(f"Python model created: {m}")

    ctx = engine.rootContext()
    ctx.setContextProperty("model", m)

    # Load the QML file
    qml_file = os.path.join(os.path.dirname(__file__), "app.qml")
    engine.load(QUrl.fromLocalFile(os.path.abspath(qml_file)))

    # Show the window
    if not engine.rootObjects():
        sys.exit(-1)

    # execute and cleanup
    sys.exit(app.exec_())
