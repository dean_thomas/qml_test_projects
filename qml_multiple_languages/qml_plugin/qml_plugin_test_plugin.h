#ifndef QML_PLUGIN_TEST_PLUGIN_H
#define QML_PLUGIN_TEST_PLUGIN_H

#include <QQmlExtensionPlugin>

class Qml_plugin_testPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
    void registerTypes(const char *uri) override;
};

#endif // QML_PLUGIN_TEST_PLUGIN_H
