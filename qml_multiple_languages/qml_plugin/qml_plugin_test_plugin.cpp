#include "qml_plugin_test_plugin.h"

#include "piechart.h"

#include <qqml.h>

void Qml_plugin_testPlugin::registerTypes(const char *uri)
{
    // @uri test
    qmlRegisterType<PieChart>(uri, 1, 0, "PieChart");
}

