# QML multiple languages

Projects in various languages importing a QML plugin written using C++.  Each language implementation shares the same `app.qml` file to demonstrate reuse across different languages.

##  app.qml

Imports the `PieChart` QML component and initiates a 2-way binding between the QML/C++ UI model and backend data model (various languages).  Pressing the button will randomly assign the piechart a new colour, this should be synchronized in the data model.

##  qml_plugin

This project contains the C++ code used to provide the QML `PieChart` component.  This component is based upon the Qt example project at https://doc.qt.io/qt-5/qtqml-tutorials-extending-qml-example.html.  This project outputs a dynamic library containing the QML plugin; the output should be copied (or softlinked) to the intended import location.  Also required is the qmldir file, which describes the QML module and lists any plugin DLLs required to be loaded.

##  qml_python_loader

This project creates a QML engine and loads the `app.qml` file using [PySide2](https://pypi.org/project/PySide2/).  Also provided is a `PieChart` Python class that forms the backend data model.  This is available in the  QML code as `model` via
```python
ctx.setContextProperty("model", PieChart())
```

### dependencies
```
$ pip install PySide2
```

## qml_cs_loader

This project creates a QML engine and loads the `app.qml` file using [Qml.Net](https://github.com/qmlnet).  Also provided is a `Model` CS class that forms the backend data model.  This is available in the  QML code as `model` via
```csharp
engine.SetContextProperty("model", new Model());
```

### dependencies
```
$ dotnet add package Qml.Net
$ dotnet add package Qml.Net.LinuxBinaries
```

**notes:** this has been tested using `Qml.net 0.10.1`; the `qml_plugin` required compilation using `Qt 5.12.2` for compatibility.
