﻿using Qml.Net;
using Qml.Net.Runtimes;

namespace QmlQuickOverview
{
    class Model 
    {
        protected string _color = "blue";

        [NotifySignal("colorChanged")]
        public string color { 
            get { return _color; } 
            set { 
                if (value != _color) {
                    _color = value;
                    System.Console.WriteLine($"Colour was set to {value}.");
                } 
            }
        }   
    }

    class QmlExample
    {
        static int Main(string[] args)
        {
            //RuntimeManager.RuntimeSearchLocation.UserDirectory
            RuntimeManager.DiscoverOrDownloadSuitableQtRuntime(RuntimeManager.RuntimeSearchLocation.UserDirectory);
            //var x= RuntimeManager.GetPotentialRuntimesDirectories(RuntimeManager.RuntimeSearchLocation.All);
            //System.Console.WriteLine(x);
            //RuntimeManager.DownloadRuntimeToDirectory("5.14.2", RuntimeTarget.LinuxX64, "/home/pixalytics/Desktop/a");
            using (var app = new QGuiApplication(args))
            {
                using (var engine = new QQmlApplicationEngine())
                {
                    engine.AddImportPath("/home/pixalytics/my_plugin_dir");
                    engine.SetContextProperty("model", new Model());

                    engine.Load(@"/home/pixalytics/qml_test_projects/qml_multiple_languages/qml_python_loader/app.qml");

                    return app.Exec();
                }
            }
        }
    }
}